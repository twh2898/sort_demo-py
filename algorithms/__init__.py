from .sorter import Sorter
from .selection_sort import SelectionSort
from .insertion_sort import InsertionSort
from .heap_sort import HeapSort
from .quick_sort import QuickSort
from .merge_sort import MergeSort

#Sorters = [ SelectionSort, InsertionSort, HeapSort, QuickSort, MergeSort ]
Sorters = [ SelectionSort, InsertionSort, HeapSort, QuickSort ]

__all__ = Sorters

