from . import Sorter

class HeapSort(Sorter):
    def heapify(self, n, i):
        self.log('Heapify', n, i, level=1, count=False)
        largest = i
        l = 2 * i + 1
        r = 2 * i + 2

        if l < n and self.comp(i, l):
            largest = l

        if r < n and self.comp(largest, r):
            largest = r
        
        if largest != i:
            self.swap(largest, i)
            self.heapify(n, largest)

    def sort(self):
        n = self.size
        for i in range(n, -1, -1):
            self.heapify(n, i)

        for i in range(n-1, 0, -1):
            self.swap(0, i)
            self.heapify(i, 0)

