from . import Sorter

class InsertionSort(Sorter):
    def sort(self):
        for i in range(1, self.size):
            key = i
            while key > 0 and self.comp(key, key - 1):
                self.swap(key, key - 1)
                key -= 1 

