from . import Sorter

class MergeSort(Sorter):
    def merge(self, low, mid, high):
        self.log('Merge', low, mid, high, level=1, count=False)
        n1 = mid - low + 1
        n2 = high - mid

        i = j = 0
        k = low
        while i < n1 and j < n2:
            # l <= r is !(r < l)
            if not self.comp(j + mid, i + low):
                self.swap(k, i + low)
                i += 1
            else:
                self.swap(k, j + mid)
                j += 1
            k += 1

        while i < n1:
            self.swap(k, i + low)
            i += 1
            k += 1

        while j < n2:
            self.swap(k, j + mid)
            j += 1
            k += 1


    def mergeSort(self, low, high):
        assert 0 <= low
        assert high <= self.size
        assert low <= high
        self.log('MergeSort', low, high, level=1, count=False)
        if high > low:
            mid = (high - low) // 2
            self.mergeSort(low, mid)
            self.mergeSort(mid, high)
            self.merge(low, mid, high)

    def sort(self):
        self.mergeSort(0, self.size)
