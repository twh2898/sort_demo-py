from . import Sorter

class QuickSort(Sorter):
    def partition(self, low, high):
        self.log('Partition', low, high, level=1)
        i = low - 1

        for j in range(low, high):
            # (l <= r) is !(r < l)
            if not self.comp(high, j):
                i += 1
                self.swap(i, j)
        self.swap(i+1, high)
        return i + 1

    def quicksort(self, low, high):
        self.log('Quicksort', low, high, level=1)
        # index, not value
        if low < high:
            pi = self.partition(low, high)
            
            self.quicksort(low, pi-1)
            self.quicksort(pi+1, high)

    def sort(self):
        low = 0
        high = self.size - 1
        self.quicksort(low, high)
