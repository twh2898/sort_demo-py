from . import Sorter

class SelectionSort(Sorter):
    def sort(self):
        for i in range(self.size):
            min_i = i
            for j in range(i+1, self.size):
                if self.comp(j, min_i):
                    min_i = j
            self.swap(min_i, i)

