from random import random
from math import ceil
import sys

class Sorter:
    '''
    Base class for all sorting algorithms.
    
    This class handles data access, logging, comparison, and step counting.
    '''

    width = 20
    logging = 0

    def __init__(self,
            data,
            log=sys.stdout,
            key=lambda e: e,
            comp=lambda lhs, rhs: lhs < rhs):
        '''
        Create a new Sorter instance and copy the static data field to be
        sorted.

        Arguments:
            data: A list object containing data to be sorted
            log: An object with a write(string) method. This can be a file
                object or sys.stdout
            key: A function which, given an entry from the data set, returns
                the value of the key field used for comparisons. The default is
                a lambda that returns the whole entry.
            comp: A function which, given the key field of two entries, will
                return the result of a boolean comparison of the two keys.
        '''
        assert Sorter.width > 0
        assert len(data) > 0
        self.data = data
        self.size = len(data)
        self.log_out = log
        self.key = key
        self.compf = comp
        self.step = 0

    def __iter__(self):
        return iter(self.data)

    def __repr__(self):
        return str(self)

    def __str__(self):
        assert Sorter.width > 0
        rows = []
        nr = ceil(self.size / Sorter.width)
        for j in range(nr):
            nc = min((self.size - j * Sorter.width), Sorter.width)
            i = j * Sorter.width
            row = ['{:2}'.format(v) for v in self.data[i:i+nc]]
            rows.append(', '.join(row))
        return '\n'.join(rows)

    def __get(self, i):
        ''' Get the entry at index i from the data field '''
        assert 0 <= i < len(self.data)
        return self.data[i]

    def log(self, *msg, level=0, count=True):
        '''
        Log a message to the log object passed to the constructor.

        Arguments:
            *msg: A variable length argument list with messages to log.
            level: The log level. If the level is less than or equal to
                Sorter.logging, the message will be logged.
            count: If True, increment the step counter by 1 for this log entry.

        >>> import sys
        >>> Sorter.logging = 1
        >>> sort = Sorter([0], log=sys.stdout)
        >>> sort.log('lvl 0', level=0)
        lvl 0
        >>> sort.log('lvl 1', level=1)
        lvl 1
        >>> sort.log('lvl 2', level=2)
        '''
        assert level >= 0
        assert Sorter.logging >= 0
        if level <= Sorter.logging:
            print(*msg, file=self.log_out)
        if count:
            self.step += 1

    def is_sorted(self):
        '''
        Returns True if the data field is sorted.
        
        >>> from random import random
        >>> data = [int(random()*100) for _ in range(100)]
        >>> sort = Sorter(data)
        >>> sort.is_sorted()
        False
        '''
        assert len(self.data) > 0
        last = None
        for e in self.data:
            if last and last > e:
                return False
            last = e
        return True

    def steps(self):
        '''Return the number of steps taken.'''
        return self.step
 
    def sort(self):
        '''This method should be implemented by derived classes.'''
        raise NotImplementedError('Call this method on a derived class')

    def comp(self, lhs, rhs):
        '''
        Given the indices of two entries in data, compare the two elements
        using their key field and the comp function passed to the constructor.
        This method will be logged with a level of 2.

        Arguments:
            lhs: The index of the entry on the left hand side
            rhs: The index of the entry on the right hand side

        Returns:
            The boolean result of the comparison
        '''
        assert 0 <= lhs <= len(self.data)
        assert 0 <= rhs <= len(self.data)
        vlhs = self.read(lhs)
        vrhs = self.read(rhs)
        self.log(f'Comparing {lhs}:{vlhs} to {rhs}:{vrhs}', level=1)
        return self.compf(self.key(vlhs), self.key(vrhs))

    def swap(self, ia, ib):
        '''
        Given the indices of two entries in data, swap the values of the two
        entries. This method will be logged with a level of 2.

        Arguments:
            ia: The index of the first entry
            ib: The index of the second entry
        '''
        assert 0 <= ia < len(self.data)
        assert 0 <= ib < len(self.data)
        va = self.__get(ia)
        vb = self.__get(ib)
        self.log(f'Swap {ia}:{va} and {ib}:{vb}', level=1)
        self.data[ia] = vb
        self.data[ib] = va

    def read(self, i):
        '''Return the entry at index i'''
        assert 0 <= i < len(self.data)
        v = self.__get(i)
        self.log(f'Reading {i}:{v}', level=2)
        return v

