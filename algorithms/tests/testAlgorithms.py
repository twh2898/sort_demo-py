import unittest
from io import StringIO
from algorithms import Sorters
from random import random


class AlgorithmsTest(unittest.TestCase):
    def setUp(self):
        self.log = StringIO()
        self.key = lambda e: e
        self.comp = lambda l, r: l < r
        self.data = [int(random()*100) for _ in range(100)]

    def helper(self, Algorithm):
        sort = Algorithm(self.data.copy(), self.log, self.key, self.comp)
        self.assertFalse(sort.is_sorted(), type(Algorithm).__name__)
        sort.sort()
        self.assertTrue(sort.is_sorted(), type(Algorithm).__name__)

    def test_all_sorting_algorithms(self):
        for sorter in Sorters:
            self.helper(sorter)

