import unittest
from io import StringIO
from algorithms import Sorter
from random import random


class SorterTest(unittest.TestCase):
    def setUp(self):
        self.log = StringIO()
        self.key = lambda e: e
        self.comp = lambda l, r: l < r
        self.data = [int(random()*100) for _ in range(100)]
        self.sort = Sorter(self.data, self.log, self.key, self.comp)

    def test_constructor_stores_parameters(self):
        self.assertIs(self.data, self.sort.data)
        self.assertEquals(len(self.data), self.sort.size)
        self.assertIs(self.log, self.sort.log_out)
        self.assertIs(self.key, self.sort.key)
        self.assertIs(self.comp, self.sort.compf)
        testStr = 'This is a test'
        self.sort.log(testStr)
        self.assertEqual(testStr + '\n', self.log.getvalue())

    def test_constructor_stores_defaults(self):
        self.assertEqual(0, self.sort.step)
    
    def test_iter_is_data(self):
        it = iter(self.sort)
        for i, v in enumerate(it):
            self.assertEqual(self.sort.data[i], v)

    def test_repr_is_str(self):
        to_str = str(self.sort)
        to_repr = repr(self.sort)
        self.assertEqual(to_str, to_repr)

    def test_log_level_bellow_logging(self):
        Sorter.logging = 2
        logtext = 'log text'
        self.sort.log(logtext, level=1)
        self.assertEqual(logtext + '\n', self.log.getvalue())

    def test_log_level_at_logging(self):
        Sorter.logging = 1
        logtext = 'log text'
        self.sort.log(logtext, level=1)
        self.assertEqual(logtext + '\n', self.log.getvalue())

    def test_log_level_above_logging(self):
        Sorter.logging = 0
        logText = 'Log Text'
        self.sort.log(logText, level=1)
        self.assertEqual('', self.log.getvalue())

    def test_log_with_count(self):
        self.sort.log(count=True)
        self.assertEqual(1, self.sort.step)

    def test_log_no_count(self):
        self.sort.log(count=False)
        self.assertEqual(0, self.sort.step)

    def test_is_sorted_and_more(self):
        self.assertFalse(self.sort.is_sorted())
        self.sort.data.sort()
        self.assertTrue(self.sort.is_sorted())

    def test_steps(self):
        self.assertEqual(0, self.sort.steps())
        self.sort.step = 2
        self.assertEqual(2, self.sort.steps())

    def test_call_sort(self):
        with self.assertRaises(NotImplementedError):
            self.sort.sort()
    
    def test_comp_small_and_big(self):
        assert len(self.data) > 1
        self.data[0] = 10
        self.data[1] = 17
        expect = self.comp(self.data[0], self.data[1])
        self.assertEqual(expect, self.sort.comp(0, 1))
        expectLog = f'Comparing 0:{self.data[0]} to 1:{self.data[1]}\n'
        self.assertEqual(expectLog, self.log.getvalue())

    def test_comp_big_and_small(self):
        assert len(self.data) > 1
        self.data[0] = 17
        self.data[1] = 10
        expect = self.comp(self.data[0], self.data[1])
        self.assertEqual(expect, self.sort.comp(0, 1))
        expectLog = f'Comparing 0:{self.data[0]} to 1:{self.data[1]}\n'
        self.assertEqual(expectLog, self.log.getvalue())
    
    def test_comp_same(self):
        assert len(self.data) > 1
        self.data[0] = 15
        self.data[1] = 15
        expect = self.comp(self.data[0], self.data[1])
        self.assertEqual(expect, self.sort.comp(0, 1))
        expectLog = f'Comparing 0:{self.data[0]} to 1:{self.data[1]}\n'
        self.assertEqual(expectLog, self.log.getvalue())

    def test_swap(self):
        assert len(self.data) > 1
        va = self.data[0]
        vb = self.data[1]
        expectLog = f'Swap 0:{self.data[0]} and 1:{self.data[1]}\n'
        self.sort.swap(0, 1)
        self.assertEqual(vb, self.data[0])
        self.assertEqual(va, self.data[1])
        self.assertEqual(expectLog, self.log.getvalue())

    def test_read(self):
        assert len(self.data) > 0
        Sorter.logging = 2
        expect = self.data[0]
        self.assertEqual(expect, self.sort.read(0))
        expectLog = f'Reading 0:{self.data[0]}\n'
        self.assertEqual(expectLog, self.log.getvalue())

