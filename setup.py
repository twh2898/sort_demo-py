#!/usr/bin/env python3

from setuptools import setup

def readme():
    with open('README.md', 'r') as fin:
        return fin.read()

setup(name='sortdemo',
        version='0.5',
        description='Sorting algorithm demo',
        long_description=readme(),
        classifiers=[
            'Development Status :: 3 - Alpha',
            'Environment :: Console',
            'Intended Audience :: Developers',
            'License :: OSI Approved :: MIT License',
            'Programming Language :: Python :: 3.6',
            'Topic :: Education'
        ],
        keywords='sorting demo algorithms',
        url='',
        author='Thomas Harrison',
        author_email='twh2898@vt.edu',
        license='MIT',
        packages=['algorithms'],
        install_requires=[],
        test_suite='nose.collector',
        tests_require=['nose', 'nose-cover3', 'rednose'],
        scripts=['sortdemo.py'],
        include_package_data=True,
        zip_safe=False)
