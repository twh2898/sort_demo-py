#!/usr/bin/env python3

from random import random
from algorithms import Sorter, Sorters


if __name__ == '__main__':
    import argparse
    import sys

    parser = argparse.ArgumentParser(description='Sorting algorithm demo.')
    parser.add_argument('-n', '--size', type=int, default=100, help='The number of elements to sort (default: %(default)s)')
    parser.add_argument('-o', '--output', type=str, default=None, help='Write the output to a file. If not specified, write to the console.')
    parser.add_argument('-w', '--width', type=int, default=10, help='Set the number of elements to print on each row of the output (default: %(default)s)')
    parser.add_argument('-v', '--verbose', action='count', help='Enable verbose logging, this includes each step taken by the sorting algorithm (default: False)')

    #args = parser.parse_args('-vv -n 100 -w 10'.split())
    #args = parser.parse_args(['-h'])
    args = parser.parse_args()

    log_to = sys.stdout
    if args.output:
        try:
            log_to = open(args.output, 'w')
        except OSError as e:
            print('Failed to open output file', args.output, file=sys.stderr)
            sys.exit(-1)

    if args.size <= 0:
        parser.error('Size must be a posative integer larger than 0.')

    Sorter.width = args.width
    Sorter.logging = args.verbose or 0
    
    print('Generating', args.size, 'random numbers', file=log_to)
    data = [int(random()*100) for _ in range(args.size)]

    first = True
    for sorter in Sorters:
        sort = sorter(data.copy(), log_to)
        if first:
            print(file=log_to)
            print(sort, file=log_to)
            first = False
        print(file=log_to)
        print('::', type(sort).__name__, file=log_to)
        print('   Sorting data', file=log_to)
        assert not sort.is_sorted()
        sort.sort()
        print('   In', sort.steps(), 'steps', file=log_to)
        if sort.is_sorted():
            print('   Data was sorted', file=log_to)
        else:
            print('!! Data was not sorted', file=log_to)
    
    if log_to != sys.stdout:
        close(log_to)

